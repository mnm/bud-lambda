package env

type Env struct {
	AWS *AWS
}

type AWS struct {
	AccessKey string `key:"AWS_ACCESS_KEY_ID"`
	SecretKey string `key:"AWS_SECRET_ACCESS_KEY"`
	Region    string `default:"us-east-2"`
}
