package deploy

import (
	"context"
	"fmt"

	"gitlab.com/mnm/bud-lambda/env"
	"gitlab.com/mnm/bud/go/mod"
)

func New(env *env.Env, modfile mod.File) *Command {
	return &Command{env, modfile}
}

type Command struct {
	env     *env.Env
	modfile mod.File
}

func (c *Command) Run(ctx context.Context) error {
	fmt.Printf("lambda: deploying %s to %s!\n", c.modfile.Directory(), c.env.AWS.Region)
	return nil
}
