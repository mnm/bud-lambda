module gitlab.com/mnm/bud-lambda

go 1.17

replace gitlab.com/mnm/bud => ../bud

require gitlab.com/mnm/bud v0.0.0-00010101000000-000000000000

require (
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898 // indirect
)
